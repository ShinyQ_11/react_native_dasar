import React, {Component} from 'react'
import {View, Text} from 'react-native'
import Coffee from './Coffee'

class Glass extends Component {

    render(){
        return(
            <View>
                <Text>Glass</Text>
                <Coffee type="Espresso" bean="Arabica"/>
            </View>
        )
    }

}

export default Glass;