import React, {Component, ReactPropTypes} from 'react'
import {View, Text} from 'react-native'

class Coffee extends Component {

    render(){

        const {type, bean} = this.props;

        return(
            <View>
                <Text>Kopi {type} dari biji {bean}</Text>
            </View>
        )
    }

}

export default Coffee;