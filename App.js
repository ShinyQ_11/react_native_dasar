import React, {Component} from 'react'
import {View, Text} from 'react-native'
import Cup from './app/component/Cup'

import Glass from './app/component/Glass'

class index extends Component {

    componentWillMount(){
      console.log('Component Will Mount')
    }

    componentWillMount(){
      console.log('Component Did Mount')
    }

    render(){
        return(
            <View>
                <Glass />
                <Cup />
            </View>
        )
    }

}

export default index;